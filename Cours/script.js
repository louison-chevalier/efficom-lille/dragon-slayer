var i;

for(i=0; i<10; i++){
	document.write(i+"<br />");
}


var j;
j=50;

while(j>10){
	document.write(j+"<br />");
	j--;
}


var sexe = window.prompt("Quel est votre sexe?");

switch(sexe){
	case "Homme":
	document.write("Vous etes un homme");
	break;
	case "Femme":
	document.write("Vous etes un femme");
	break;
}


var maVoiture = new Object();
maVoiture.marque = "Audi";
maVoiture.annee = 2015;

document.write(maVoiture.marque);




function carredecinq(){
	var x = 5;
	var resultat = x*x;
	document.write(resultat);
}
carredecinq();



function carre(x){
	var resultat = x*x;
	return resultat;
}


var premierCarre = carre(10);
document.write(premierCarre);


/*
  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/random
  La fonction Math.random() renvoie un nombre flottant pseudo-aléatoire compris dans l'intervalle [0, 1[ (ce qui signifie que 0 est compris dans l'intervalle mais que 1 en est exclu)

  Ce nombre peut ensuite être multiplié afin de couvrir un autre intervalle.
  Nota : Dans l'exemple correspondant, on ramène min/max à une valeur entière
    min = 5.2 -> 6
    max = 8.3 -> 8

  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/floor
  La fonction Math.floor(x) renvoie le plus grand entier qui est inférieur ou égal à un nombre x.

  Math.floor(5.95) -> 5

  https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/ceil
  La fonction Math.ceil() retourne le plus petit entier supérieur ou égal au nombre donné.

  Math.ceil(7.004) -> 8
*/
